FROM containers.torproject.org/tpo/tpa/base-images/python:bookworm

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  WORKDIR=/home/anonticket/anon_ticket \
  VIRTUAL_ENV=/home/anonticket/.env \
  PATH="/home/anonticket/.env/bin:$PATH" \
  CSRF_TRUSTED_ORIGIN="http://localhost:8000" \
  DEBUG=False \
  SECRET_KEY=CHANGEME \
  ALLOWED_HOSTS=".anonticket.torproject.org," \
  GITLAB_ACCOUNTS_SECRET_TOKEN=CHANGEME \
  GITLAB_SECRET_TOKEN=CHANGEME \
  GITLAB_URL="https://gitlab.torproject.org/" \
  AUTO_ACCEPT_LIST="" \
  BLOCK_ALL=False \
  GITLAB_TIMEOUT=10 \
  LIMIT_RATE=60/m \
  MAIN_RATE_GROUP=main_rate_bucket \
  TIMEOUT_URL="http://10.0.0.0/"
  
# run-time dependencies
RUN apt-get update && \
  apt-get install -y --no-install-recommends \
    python-is-python3 \
    python3-poetry \
    && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Create unprivileged anonticket user/group
RUN groupadd -r -g 999 anonticket && \
  useradd --no-log-init -r -m -u 999 -g anonticket anonticket

# Work in application directory
WORKDIR /home/anonticket/anon_ticket

# Expose port 8000/tcp for gunicorn
EXPOSE 8000

# Copy all project files (minus those ignored)
COPY --chown=anonticket:anonticket . .

# Create virtualenv and install anonticket app
# then make home directory owned by the right user
RUN --mount=type=cache,target=/root/.cache/pypoetry/cache \
  --mount=type=cache,target=/root/.cache/pypoetry/artifacts \
  --mount=type=cache,target=/root/.cache/pip \
  python3 -m venv /home/anonticket/.env && \
  poetry install --without=dev --no-interaction --no-ansi && \
  poetry run ./manage.py makemigrations && \
  chown anonticket:anonticket -R /home/anonticket

# Switch to anonticket user
USER anonticket

# Launch the app
CMD ["./launch.sh"]
