coverage==7.2.5  # 5.3.1
Django==4.2.6 # cve: 3.1.6
django-markdownify==0.8.2
django-ratelimit==3.0.1
django-test-plus==1.4.0
djangorestframework==3.15.2  # cve: 3.12.4
gunicorn==22.0.0  # cve: 20.0.4
python-decouple==3.8  # 3.4
python-gitlab==2.10.1  # 2.5.0
psycopg2-binary==2.9.9
