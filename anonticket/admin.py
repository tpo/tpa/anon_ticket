from django.contrib import admin
from django.db import models
from django.forms import Textarea

# Register your models here.

from .models import UserIdentifier, Project, Issue, Note, GitlabAccountRequest
admin.site.register(UserIdentifier)
admin.site.register(Project)

def bulk_approve_issues(modeladmin, request, queryset):
    """Add a bulk approval method for issues to admin panel."""
    for issue in queryset:
        issue.reviewer_status = 'A'
        issue.save()

bulk_approve_issues.short_description = "Approve selected issues and post to GitLab."

def bulk_approve_notes(modeladmin, request, queryset):
    """Add a bulk approval method for issues to admin panel."""
    for note in queryset:
        note.reviewer_status = 'A'
        note.save()

bulk_approve_notes.short_description = "Approve selected issues and post to GitLab."

@admin.register(Issue)
class IssueModelAdmin(admin.ModelAdmin):
    list_display = ('title', 'linked_project', 'confidential', 'reviewer_status')
    list_filter = ('reviewer_status', )
    actions = [bulk_approve_issues]

@admin.register(Note)
class NoteModelAdmin(admin.ModelAdmin):
    list_display = ('body', 'linked_project','reviewer_status')
    list_filter = ('reviewer_status', )
    actions = [bulk_approve_notes]

@admin.register(GitlabAccountRequest)
class GitlabAccountRequestAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'address', 'mod_comment',
                    'reviewer_status', 'rejection_reason')
    list_editable = ('mod_comment', 'reviewer_status', 'rejection_reason')
    formfield_overrides = {
        # To reduce the size of the TextField
        models.TextField: {'widget': Textarea(attrs={'rows': 10, 'cols': 30})},
    }
