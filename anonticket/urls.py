from django.urls import path, include, reverse, re_path
from django.views.generic import (
    TemplateView, DetailView, ListView, CreateView, UpdateView)
from . import views
from django.conf import settings
from anonticket.views import (
    CreateIdentifierView, 
    IssueSuccessView,
    ObjectCreatedNoUserView,
    UserLoginErrorView,
    GitlabAccountRequestCreateView,
    CannotCreateObjectView,
    ProjectListView,
    ProjectDetailView,
    PendingIssueDetailView,
    NoteCreateView,
    ModeratorNoteUpdateView,
    ModeratorIssueUpdateView,
    ModeratorGitlabAccountRequestUpdateView,
    )


urlpatterns = [
    path(
        '', 
        TemplateView.as_view(template_name="anonticket/index.html"), 
        name='home'),
    path(
        "robots.txt",
        TemplateView.as_view(
            template_name="anonticket/robots.txt",
            content_type="text/plain"),
    ),
    path(
        "BingSiteAuth.xml",
        TemplateView.as_view(
            template_name="anonticket/BingSiteAuth.xml",
            content_type="text/xml"),
    ),
    path(
        'user/create_identifier/', 
        views.CreateIdentifierView.as_view(), 
        name='create-identifier'),
    path(
        'user/login/', 
        views.login_view, name='login'),
    path(
        'user/logout/',
        views.logout_view, name='logout'),
    path(
        'user/email-domain-rejected/',
        views.EmailDomainRejectedView.as_view(),
        name='email-domain-rejected'),
    path(
        'user/create-failed/',
        views.CannotCreateObjectView.as_view(),
        name='cannot-create-with-user'),
    path(
        'user/gitlab-account/create/', 
        views.GitlabAccountRequestCreateView.as_view(), 
        name='create-gitlab'),
    path(
        'user/login_error/', 
        views.UserLoginErrorView.as_view(), name='user-login-error'),
    path(
        'user/projects/<slug:project>/issues/<int:issue_iid>/notes/create/', 
        views.NoteCreateView.as_view(), 
        name='create-note'),
    path(
        'user/projects/<slug:project>/issues/<int:issue_iid>/notes/<int:pk>/', 
        views.PendingNoteDetailView.as_view(), name='pending-note'),
    path(
        'user/projects/<slug:project_slug>/issues/<int:gitlab_iid>/details/<int:go_back_number>/', 
        views.issue_detail_view, name='issue-detail-view-go-back'),
    path(
        'user/projects/<slug:project_slug>/issues/<int:gitlab_iid>/details/', 
        views.issue_detail_view, name='issue-detail-view'),
    path(
        'user/projects/<slug:project_slug>/issues/pending/<int:pk>/', 
        views.PendingIssueDetailView.as_view(), name='pending-issue-detail-view'),
    path(
        'user/projects/all/issues/search/', 
        views.issue_search_view, name="issue-search"),
    path(
        'user/projects/<slug:slug>/page/<int:page_number>', 
        views.ProjectDetailView.as_view(), name='project-detail'),
    path(
        'user/projects/', 
        views.ProjectListView.as_view(), name='project-list'),
    path(
        'user/create/success/', 
        views.IssueSuccessView.as_view(), name='issue-created'),
    path(
        'user/create/success/no_user', 
        views.ObjectCreatedNoUserView.as_view(), name='created-no-user'),         
    path(
        'user/create_issue/', 
        views.create_issue_view, name='create-issue'),
    path(
        'user/', 
        views.user_landing_view, name='user-landing'),
    # Catch all any other `user/X` URL and return 410 Gone
    re_path(r'^user/.*$', views.user_gone, name='user-gone'),
    path(
        'moderator/', 
        views.moderator_view, name='moderator'),
    path(
        'moderator/update-note/<int:pk>', 
        views.ModeratorNoteUpdateView.as_view(), 
        name='mod-update-note'),
    path(
        'moderator/update-issue/<int:pk>', 
        views.ModeratorIssueUpdateView.as_view(), 
        name='mod-update-issue'),
    path(
        'moderator/update-gitlab-account-request/<int:pk>', 
        views.ModeratorGitlabAccountRequestUpdateView.as_view(), 
        name='mod-update-gitlab-account-request'),
]
