from django.urls import path
from .views import list_irc_pending


urlpatterns = [
    path('', list_irc_pending, name="irc-pending")
]