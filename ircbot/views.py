from rest_framework.decorators import api_view
from rest_framework.response import Response
from anonticket.models import Issue, Note, GitlabAccountRequest

#Function based view iwth Api-view decorator
@api_view()
def list_irc_pending(request):
    """View to list Pending API totals. GET-only accepted."""
    issue_total = Issue.objects.filter(reviewer_status='P').count()
    note_total = Note.objects.filter(reviewer_status='P').count()
    gitlab_account_request_total = GitlabAccountRequest.objects.filter(reviewer_status='P').count()
    build_dictionary = {
        "pending_issues": issue_total,
        "pending_notes": note_total,
        "pending_gitlab_account_requests": gitlab_account_request_total,
    }
    return Response(build_dictionary)

